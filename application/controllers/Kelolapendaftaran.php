<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelolapendaftaran extends CI_Controller {

    public function __construct(){
        parent::__construct();
        is_login();
        $this->load->model('Login_model');
		
	}

    public function index(){ 
        $get = $this->db->query("SELECT a.*, b.kode_kavling as pilihanmnya FROM list_pemohon a
        INNER JOIN list_produk b ON a.kavling_pilihan = b.id
        WHERE deleted_at IS NULL")->result();  
        $datatable = array();
        $no=0;
        foreach ($get as $key => $value) {

            $masa = date('Y') - date('Y', strtotime($value->tgl_sk_pns));
        $no++;
            $action =
            '<div class="row">
			 <a href="'. base_url('kelolapendaftaran/verifikasi/') . $value->id .'" ><span class="badge badge-info">'.$value->nip.'</span></a>
             </div>';

             $hps =
            '<div class="row">
            <a href="'. base_url('kelolapendaftaran/hapus/') . $value->id .'"  onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\');"><span class="badge badge-danger">Hapus</span></a>
             </div>';

             $file =
            '<div class="row">
			 <a target="BLANK" href="'. base_url($value->persyaratan) .'" ><span class="badge badge-warning"> Lihat File </span></a>
             </div>';


             $datatable[$key] = array(
                'no'    => $no,
                'nama'    => $value->nama,
                'nip'     =>  $action,
                'tm_lhr'  => $value->tm_lhr,
                'tg_lhr'  => $value->tg_lhr,
                'alamat'  => $value->alamat,
                'instansi'=> $value->instansi,
                'unit'    => $value->unit,
                'tgl_sk_cpns' => $value->tgl_sk_cpns,
                'tgl_sk_pns'  => $value->tgl_sk_pns.' ('. $masa .' Tahun )',
                'gol'     => $value->gol,
                'nik'     => $value->nik,
                'email'   => $value->email,
                'telp'    => $value->telp,
                'pasangan'=> $value->pasangan,
                'gaji'    => number_format($value->gaji),
                'per'   => $value->bulan.'-'.$value->tahun,
                'kavling_pilihan' => $value->pilihanmnya,
                'persyaratan' => $file,
                'status'    => status($value->status),
                'aksi'    => $hps,
                'created_at'=> $value->created_at,
            );
        }
        $data['datatable'] = $datatable;     
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('pendaftaran/index', $data);   
    }

    public function verifikasi($id){ 
        $get = $this->db->query("SELECT a.*, b.kode_kavling as pilihanmnya FROM `list_pemohon` a  INNER JOIN list_produk b ON a.kavling_pilihan = b.id WHERE a.id = '$id'")->row();
        $data['list_kavling'] = $this->db->query("SELECT a.* FROM list_produk a
        INNER JOIN periode_penjualan b ON a.periode_id = b.id
        WHERE b.status = 1 AND b.tanggal_tutup < CURDATE()
        ")->result();
        $data['pns'] = $get;     
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('pendaftaran/edit', $data);   
    }

    public function act_verifikasi(){ 
        $id = $this->input->post('id',true);
        $status = $this->input->post('status', true);
        $this->db->where('id', $id);
        $this->db->update('list_pemohon', array('status' => $status));
        $this->session->set_flashdata('info', 'success');
        $this->session->set_flashdata('message', 'Update Status Verifikasi Berhasil');
        redirect('kelolapendaftaran'); 
    }

    public function hapus($id)
    {
        $row = $this->Login_model->get_by_id($id);
        if ($row) {
            $this->Login_model->delete($id);
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'selamat, berhasil ');
            redirect('kelolapendaftaran'); 
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'gagal hapus ');
            redirect('kelolapendaftaran'); 
        }
    }

    
}
?>