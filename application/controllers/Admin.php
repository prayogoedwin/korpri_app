<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // is_login();
		
	}

    public function index(){        
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('dashboard');   
    }

    public function admin(){        
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('dashboard');   
    }


}