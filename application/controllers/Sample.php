<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sample extends CI_Controller {

    public function __construct(){
		parent::__construct();
		
	}

    public function index(){        
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('sample/dashboard');   
    }

    public function form(){        
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('sample/form');   
    }

    public function table(){        
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('sample/table');   
    }


}