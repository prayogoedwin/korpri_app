<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        is_login();
		
	}

    public function index(){    
        $data['total_kav'] = $this->db->query("SELECT count(id) as a FROM list_produk WHERE periode_id = (SELECT max(periode_id) FROM list_produk);")->row();  
        $data['total_pemohon'] = $this->db->query("SELECT count(a.id) as a FROM list_pemohon a INNER JOIN list_produk b ON a.kavling_pilihan = b.id WHERE b.periode_id = (SELECT max(periode_id) FROM list_produk);")->row();  
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('dashboard', $data);   
    }

    // public function menu(){        
    //     $this->load->view('template/head');
    //     $this->load->view('template/header');
    //     $this->load->view('template2/sidebar');
    //     $this->load->view('dashboard');   
    // }

    // public function profil($url){  
    //     $this->load->view('template/head');
    //     $this->load->view('template/header');
    //     $this->load->view('template2/sidebar');
    //     $this->load->view('profil/'.$url);   
    // }


}