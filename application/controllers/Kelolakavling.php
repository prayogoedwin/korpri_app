<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelolakavling extends CI_Controller {

    public function __construct(){
        parent::__construct();
        is_login();
        // $this->load->model('Profil_model');
		
	}

    public function index(){ 
        $get = $this->db->query("SELECT a.*, b.nama as dipesan FROM list_produk a
        LEFT JOIN list_pemohon b ON a.id = b.kavling_pilihan
        WHERE a.periode_id = (SELECT max(periode_id) FROM list_produk)
        ORDER BY a.id;")->result(); 
        
        
        // SELECT * FROM list_produk 
        // WHERE periode_id = (SELECT max(periode_id) FROM list_produk);")->result(); 

        $datatable = array();
        $no=0;
        foreach ($get as $key => $value) {
        $no++;
           
             $datatable[$key] = array(
                'no'            => $no,
                'periode'       => $value->periode_id,
                'blok'          => $value->blok,
                'kode_kavling'  => $value->kode_kavling,
                'luas_kavling'  => $value->luas_kavling,
                'harga_jual'    => $value->harga_jual,
                'dipesan'       => $value->dipesan,
            );
        }
        $data['datatable'] = $datatable;     
        $this->load->view('template/head');
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('kavling/index', $data);   
    }

    
}
?>