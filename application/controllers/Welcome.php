<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){  
        $this->load->view('template/head');
        $periode = $this->db->query("SELECT * FROM periode_penjualan WHERE id = (SELECT max(id) FROM periode_penjualan) AND `status` = 1")->row();
        if($periode->tanggal_tutup < date('Y-m-d')){
          $this->session->set_flashdata('info', 'warning');
          $this->session->set_flashdata('message', '<h3>Pendaftaran Sudah Ditutup Tanggal '.date('d-m-Y', strtotime($periode->tanggal_tutup)).'</h3>');
          $this->load->view('info');   
        }else{
          $this->load->view('cek');   
        }
       
  }

//   public function login(){  
//     $this->load->view('template/head');
//     $this->load->view('login');   
// }

  public function berhasil(){  
    $this->load->view('template/head');
    // $this->session->set_flashdata('info', 'warning');
    // $this->session->set_flashdata('message', '<h1>Pendaftaran Berhasil</H1><br/>Terima kasih, data Anda sudah Kami terima. ');
    $this->load->view('info');  
}
  
  public function pendaftaran2(){ 
    $nip = $this->input->post('nip');

    $endpoint = 'profil';
		$params = array(
			'nip' => $nip
    );
        
    $result = post_data($endpoint, json_encode($params));
    $object = json_decode($result, TRUE);

    $nips = $object['results'][1]['value'];
    $data['nama'] =  $object['results'][0]['value'];
    $data['nip'] =   $nips;
    $data['tm_lhr'] =  $object['results'][2]['value'];
    $data['tg_lhr'] =  $object['results'][3]['value'];
    $data['alamat'] =  $object['results'][4]['value'];
    $data['tgl_sk_cpns'] =  $object['results'][11]['value'];
    $data['tgl_sk_pns'] =  $object['results'][13]['value'];
    $data['gol'] =  $object['results'][14]['value'];
    $data['nik'] =  $object['results'][25]['value'];

    // echo $object['results'][14]['value'];

    if($nips == ''){
      $this->session->set_flashdata('info', 'danger');
      $this->session->set_flashdata('message', 'NIP yang Anda input salah');
      redirect('welcome'); 
    }else{
    // $hasil = $object['results'];
      $data['list_kavling'] = $this->db->query("SELECT a.* FROM list_produk a
      INNER JOIN periode_penjualan b ON a.periode_id = b.id
      WHERE b.status = 1 AND b.tanggal_tutup > CURDATE()
      ")->result();
      $this->load->view('template/head');
      $this->load->view('regis', $data);   
    }
  }

  public function pendaftaran(){ 
    $nip = $this->input->post('nip');
    
    $endpoint = 'profil';
		$params = array(
			'nip' => $nip
    );
        
    $result = post_data($endpoint, json_encode($params));
    $object = json_decode($result, TRUE);

    $nips = $object['results'][1]['value'];
    $data['nama'] =  $object['results'][0]['value'];
    $data['nip'] =   $nips;
    $data['tm_lhr'] =  $object['results'][2]['value'];
    $data['tg_lhr'] =  $object['results'][3]['value'];
    $data['alamat'] =  $object['results'][4]['value'];
    $data['tgl_sk_cpns'] =  $object['results'][11]['value'];
    $data['tgl_sk_pns'] =  $object['results'][13]['value'];
    $data['gol'] =  $object['results'][14]['value'];
    $data['nik'] =  $object['results'][25]['value'];

    // echo $object['results'][14]['value'];

    if($nips == ''){
      $this->session->set_flashdata('info', 'danger');
      $this->session->set_flashdata('message', 'NIP yang Anda input salah');
      redirect('welcome'); 
    }else{
    // $hasil = $object['results'];
      $data['list_kavling'] = $this->db->query("SELECT a.* FROM list_produk a
      INNER JOIN periode_penjualan b ON a.periode_id = b.id
      WHERE b.status = 1 AND b.tanggal_tutup > CURDATE()
      ")->result();
      $this->load->view('template/head');
      $this->load->view('regis', $data);   
    }
  }

  public function daftar(){ 
    $nip = $this->input->post('nip');

    if($nip == ''){
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'NIP Tidak Ditemukan');
            redirect('welcome'); 
    }else{

      $cek = $this->db->query("SELECT COUNT(nip) AS c FROM list_pemohon WHERE nip = '$nip' ")->row();
      // echo $cek->c;
      if($cek->c == 0){
        $nama = $this->input->post('nama', TRUE);    
        $tm_lhr = $this->input->post('tm_lhr', TRUE);
        $tg_lhr = $this->input->post('tg_lhr', TRUE);
        $alamat = $this->input->post('alamat', TRUE);
        $tgl_sk_cpns = $this->input->post('tgl_sk_cpns', TRUE);
        $tgl_sk_pns = $this->input->post('tgl_sk_pns', TRUE);
        $gol = $this->input->post('gol', TRUE);
        $nik = $this->input->post('nik', TRUE);
        $email = $this->input->post('email', TRUE);
        $telp = $this->input->post('no_hp', TRUE);
        $pasangan = $this->input->post('pasangan', TRUE);
        $pilihan = $this->input->post('pilihan', TRUE);
        
        $endpoint = 'kinerja';
        $params = array(
          'nip'  => $nip,
          'bulan'=> (date('m') - 1),
          'tahun'=> date('Y')
        );
            
        $result = post_data($endpoint, json_encode($params));
        $object = json_decode($result, TRUE);
        
        
        $gaji =  $object['gaji'];
        $skpd =  $object['skpd_name'];
        $unit =  $object['unit_name'];
        $bulan =  $object['bulan'];
        $tahun =  $object['tahun'];


        //pdf
        ini_set('max_execution_time', 1000);
        $filename = null;

        $config['upload_path']   = './assets/upload/persyaratan/' . date('m_Y');
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;	
        // $config['max_size'] = '500';

        if (!file_exists($config['upload_path'])) {
          mkdir($config['upload_path'], 0777, true);
        }

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('filez') || empty($_FILES['filez']['name'])) {
         
            $upload = $this->upload->data();
            $filename = '/assets/upload/persyaratan/'.date('m_Y').'/'.$upload['file_name'];
          

                $input = array(
                    'nama'    => $nama,
                    'nip'     => $nip,
                    'tm_lhr'  => $tm_lhr,
                    'tg_lhr'  => $tg_lhr,
                    'alamat'  => $alamat,
                    'instansi'=> $skpd,
                    'unit'    => $unit,
                    'tgl_sk_cpns' => $tgl_sk_cpns,
                    'tgl_sk_pns'  => $tgl_sk_pns,
                    'gol'     => $gol,
                    'nik'     => $nik,
                    'email'   => $email,
                    'telp'    => $telp,
                    'pasangan'=> $pasangan,
                    'gaji'    => $gaji,
                    'bulan'   => $bulan,
                    'tahun'   => $tahun,
                    'kavling_pilihan' => $pilihan,
                    'persyaratan' => $filename,
                    'created_at'=> date('Y-m-d H:i:s')
                );
                $save = $this->db->insert('list_pemohon', $input);
                if($save){
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('message', '<h1>Pendaftaran Berhasil</H1><br/>Terima kasih, data Anda sudah Kami terima. ');
                    redirect('welcome/berhasil'); 
                    
                }else{
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message', 'Pendaftaran Gagal, ulangi beberapa saat lagi');
                    redirect('welcome'); 
                }

        } else {
          $this->session->set_flashdata('info', 'danger');
          $this->session->set_flashdata('message', 'Pendaftaran Gagal, kendala saat upload file persyaratan.');
          redirect('welcome'); 
        }

      }else{
        // echo "gagal";
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Pendaftaran Gagal,Anda sudah pernah mendaftar sebelumnya');
            redirect('welcome'); 
      }

      }

    // echo $object['results'][14]['value'];

    // $hasil = $object['results'];

    // $data['list_kavling'] = $this->db->query("SELECT * FROM list_produk")->result();

    // $this->load->view('template/head');
    // $this->load->view('regis', $data);   
  }

  public function daftar2(){ 
    $nip = $this->input->post('nip');

    if($nip == ''){
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'NIP Tidak Ditemukan');
            redirect('welcome'); 
    }else{

      $cek = $this->db->query("SELECT COUNT(nip) AS c FROM list_pemohon WHERE nip = '$nip' ")->row();
      // echo $cek->c;
      if($cek->c == 0){
        $nama = $this->input->post('nama', TRUE);    
        $tm_lhr = $this->input->post('tm_lhr', TRUE);
        $tg_lhr = $this->input->post('tg_lhr', TRUE);
        $alamat = $this->input->post('alamat', TRUE);
        $tgl_sk_cpns = $this->input->post('tgl_sk_cpns', TRUE);
        $tgl_sk_pns = $this->input->post('tgl_sk_pns', TRUE);
        $gol = $this->input->post('gol', TRUE);
        $nik = $this->input->post('nik', TRUE);
        $email = $this->input->post('email', TRUE);
        $telp = $this->input->post('no_hp', TRUE);
        $pasangan = $this->input->post('pasangan', TRUE);
        $pilihan = $this->input->post('pilihan', TRUE);
        
        $endpoint = 'kinerja';
        $params = array(
          'nip'  => $nip,
          'bulan'=> (date('m') - 1),
          'tahun'=> date('Y')
        );
            
        $result = post_data($endpoint, json_encode($params));
        $object = json_decode($result, TRUE);
        
        
        $gaji =  $object['gaji'];
        $skpd =  $object['skpd_name'];
        $unit =  $object['unit_name'];
        $bulan =  $object['bulan'];
        $tahun =  $object['tahun'];


        $input = array(
            'nama'    => $nama,
            'nip'     => $nip,
            'tm_lhr'  => $tm_lhr,
            'tg_lhr'  => $tg_lhr,
            'alamat'  => $alamat,
            'instansi'=> $skpd,
            'unit'    => $unit,
            'tgl_sk_cpns' => $tgl_sk_cpns,
            'tgl_sk_pns'  => $tgl_sk_pns,
            'gol'     => $gol,
            'nik'     => $nik,
            'email'   => $email,
            'telp'    => $telp,
            'pasangan'=> $pasangan,
            'gaji'    => $gaji,
            'bulan'   => $bulan,
            'tahun'   => $tahun,
            'kavling_pilihan' => $pilihan,
            'created_at'=> date('Y-m-d H:i:s')
        );
        $save = $this->db->insert('list_pemohon', $input);
        if($save){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', '<h1>Pendaftaran Berhasil</H1><br/>Terima kasih, data Anda sudah Kami terima. ');
            redirect('welcome/berhasil'); 
            
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Pendaftaran Gagal, ulangi beberapa saat lagi');
            redirect('welcome'); 
        }
      }else{
        // echo "gagal";
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Pendaftaran Gagal,Anda sudah pernah mendaftar sebelumnya');
            redirect('welcome'); 
      }

      }

    // echo $object['results'][14]['value'];

    // $hasil = $object['results'];

    // $data['list_kavling'] = $this->db->query("SELECT * FROM list_produk")->result();

    // $this->load->view('template/head');
    // $this->load->view('regis', $data);   
  }

  
	
	public function listKecamatan(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_kota = $this->input->post('id_kota');
        
        $kecamatan = $this->Auto_model->get_kecamatan_by_kota($id_kota);
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option value=''>Silakan Pilih Kecamatan</option>";
        
        foreach($kecamatan as $kec){
          $lists .= "<option value='".$kec->id."'>".$kec->name."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_kecamatan'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
  }
  
  public function listKegunaan(){
    // Ambil data ID Provinsi yang dikirim via ajax post
    $id_klasifikasi = $this->input->post('id_klasifikasi');
    
    $kegunaan = $this->Auto_model->get_kegunaan_by_klasifikasi($id_klasifikasi);
    // Buat variabel untuk menampung tag-tag option nya
    // Set defaultnya dengan tag option Pilih
    $lists = "<option value=''>Silakan Pilih Kegunaan</option>";
    
    foreach($kegunaan as $keg){
      $lists .= "<option value='".$keg->id_kegunaan."'>".$keg->nama."</option>"; // Tambahkan tag option ke variabel $lists
    }

    $lists .= "<option value='1'>Custom</option>";
    
    $callback = array('list_kegunaan'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
    echo json_encode($callback); // konversi varibael $callback menjadi JSON
 }

  public function dataUsulan(){
    $kegunaan = $this->input->post('kegunaan');
    $keg = $this->db->query("SELECT * FROM kegunaan WHERE id_kegunaan = '$kegunaan'")->row();

    if($keg){
      echo 'Rp.'. number_format($keg->jumlah_maksimal);
    }else{
      echo '-';
    }
   
    
}
	
	public function listDesa(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_kecamatan = $this->input->post('id_kecamatan');
        
        $desa = $this->Auto_model->get_desa_by_kecamatan($id_kecamatan);
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
          $lists = "<option value=''>Silakan Pilih Kelurahan</option>";
        
        foreach($desa as $ds){
          $lists .= "<option value='".$ds->id."'>".$ds->name."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_desa'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }
}
