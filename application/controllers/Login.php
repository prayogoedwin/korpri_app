<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}

    public function index(){        
        $this->load->view('login');   
    }

    public function cek(){        
        $username = $this->input->post('username');
        $password = $this->input->post('password'); 
        $cek = $this->Login_model->login($username, md5($password));
        if ($cek != FALSE){
            //login sukses
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Login Berhasil');
            $this->session->set_userdata('id', $cek->id);
            $this->session->set_userdata('username', $cek->username);
            $this->session->set_userdata('akses', $cek->akses);
            redirect('dashboard');
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Login Gagal');
            redirect('auth');
        }
    }

    public function logout(){        
        $this->session->sess_destroy();
        redirect('login');
    }

    public function cek_(){
		$username = $this->input->post('username');
        $password = $this->input->post('password'); 
        $cek = $this->Login_model->login($username, $password);
        if ($cek != FALSE){
            $now = date('Y-m-d H:i:s');
            $token = sha1($cek->email_user.$now);
            $update_act = array(
                'api_token_user'  => $token,
                'last_login_user' => $now
            );
            if($cek->cabang_id != FALSE){
                $cab = $this->Login_model->cabang($cek->cabang_id);
                $instansi = array(
                    'koperasi'  => $cab->nama_kop,
                    'kode_kop'  => $cab->kode_kop,
                    'cabang'    => $cab->nama_cab,
                    'kode_cab'  => $cab->kode_cab,
                    
                ); 
            }else{
                $kop = $this->Login_model->koperasi($cek->koperasi_id);
                $instansi = array(
                    'koperasi'  => $kop->nama_kop,
                    'kode_kop'  => $kop->kode_kop,
                    'cabang'    => NULL,
                    'kode_cab'  => NULL,
                   

                );
            }
            $update_activity = $this->Login_model->update_token_last($cek->id_user, $update_act);
            if($update_activity == TRUE ){
                $data = array(
                    'id_user'  			=> $cek->id_user,
                    'koperasi_id'  	    => $cek->koperasi_id,
                    'cabang_id'     	=> $cek->cabang_id,
                    'username_user'     => $cek->username_user,
                    'email_user'     	=> $cek->email_user,
                    'nama_user'         => $cek->nama_user,
                    'role_user'  		=> $cek->role_user,
                    'instansi'          => $instansi
                );
                $response = array(
                    'status' => TRUE,
                    'message' => 'LOGIN BERHASIL',
                    'token'  => $token,
                    'data'   => $data,
                    
                );
            }else{
                $response = array(
                    'status' => TRUE,
                    'message' => 'LOGIN GAGAL',
                    'token'  => NULL,
                    'data'   => NULL
                    
                );
            }
        }else{
            $response = array(
                'status' => TRUE,
                'message' => 'LOGIN GAGAL',
                'token'  => NULL,
                'data'   => NULL
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        
    }



}