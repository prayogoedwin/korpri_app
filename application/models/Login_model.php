<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->table = 'user';

    }

    public function login($username, $password){
        $sql = "SELECT * 
        FROM user  WHERE username = ?";
        $query = $this->db->query($sql, array($username));
        if ($query->num_rows() > 0) {
			$user = $query->row();
			if($password == $user->password) {
                $data = $user;
			} else {
				$data = FALSE;
			}
		} else {
			$data = FALSE;
        }
        return $data;

    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('list_pemohon')->row();
    }

    // delete data
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('list_pemohon');
    }
    
}