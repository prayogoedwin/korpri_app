<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="<?=base_url()?>assets/jateng.png" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold"><?=strtoupper($this->session->userdata('username'))?></div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> 	KORPRI PROVNISI JAWA TENGAH
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <?php 
        
        $satu =  $this->uri->segment('1');
        $dua = $this->uri->segment('2');

        ?>


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                
                
                <li class="nav-item">
                    <a href="<?=base_url('dashboard')?>" class="nav-link <?=cek_one($satu, 'dashboard')?>">
                        <i class="icon-display4"></i>
                        <span>
                            Dashboard
                        </span>
                    </a>
                </li>
                


               

                <!-- <li class="nav-item nav-item-submenu  ">
                    <a href="#" class="nav-link"><i class="icon-design"></i> <span>Profil</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Sidebars">

                    <li class="nav-item">
                        <a href="#" class="nav-link <">
                            <i class="icon-design"></i>
                            <span>
                                Profil Lembaga
                        </span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link <">
                            <i class="icon-design"></i>
                            <span>
                                Struktur Organisasi
                        </span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link <">
                            <i class="icon-design"></i>
                            <span>
                                Tugas & Fungsi
                        </span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link <">
                            <i class="icon-design"></i>
                            <span>
                                Tugas & Fungsi
                        </span>
                        </a>
                    </li>
                    

                    
                        
                        
                        <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-design"></i>
                            <span>
                                Lihat Data
                        </span>
                        </a>
                    </li>
                        
                    </ul>
                </li> -->

               

               

               

               


                <li class="nav-item">
                    <a href="<?=base_url('kelolapendaftaran')?>" class="nav-link <?=cek_one($satu, 'kelolapendaftaran')?>">
                        <i class="icon-users"></i>
                        <span>
                             List Pendaftaran
                        </span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=base_url('kelolakavling')?>" class="nav-link <?=cek_one($satu, 'kelolakavling')?>">
                        <i class="icon-file-excel"></i>
                        <span>
                             Data Kavling 
                        </span>
                    </a>
                </li>

         



                    <!-- <li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-tree5"></i> <span>Menu levels</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Menu levels">
								
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> Second level with child</a>
									<ul class="nav nav-group-sub">
										
										<li class="nav-item nav-item-submenu">
											<a href="#" class="nav-link"><i class="icon-apple2"></i> Third level with child</a>
											<ul class="nav nav-group-sub">
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-html5"></i> Fourth level</a></li>
												<li class="nav-item"><a href="#" class="nav-link"><i class="icon-css3"></i> Fourth level</a></li>
											</ul>
										</li>
										
									</ul>
								</li>
								
							</ul>
						</li> -->
                
                <!-- /main -->

                <!-- Forms -->
                <!-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Forms</div> <i class="icon-menu" title="Forms"></i></li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-pencil3"></i> <span>Form components</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Form components">
                        <li class="nav-item"><a href="form_inputs.html" class="nav-link">Basic inputs</a></li>
                        <li class="nav-item"><a href="form_checkboxes_radios.html" class="nav-link">Checkboxes &amp; radios</a></li>
                        
                    </ul>
                </li> -->
                
                
                <!-- /forms -->

                
                
                

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->
    
</div>
<!-- /main sidebar -->