<!-- Main content -->
<div class="content-wrapper">




<!-- Content area -->
<div class="content pt-0">

				
<!-- Dashboard content -->
<div class="row">
    <div class="col-xl-12">
    <br/>
        <!-- Quick stats boxes -->
         <div class="row">
            <div class="col-lg-12" style="text-align:center">
            <br/>

               

			  <img src="<?=base_url()?>assets/jatengs.png" width="400px">
			 
             
            

			</div>
			
        </div>
        <!-- /quick stats boxes -->

        <!-- Quick stats boxes -->
						<div class="row">
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="card bg-teal-400">
									<div class="card-body">
										<div class="d-flex">
											<h3 class="font-weight-semibold mb-0"><?=$total_kav->a?></h3>
											<span class="badge bg-teal-800 badge-pill align-self-center ml-auto"></span>
					                	</div>
					                	
					                	<div>
											Total Kavling
											<div class="font-size-sm opacity-75"></div>
										</div>
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>

							<div class="col-lg-4">

								<!-- Members online -->
								<div class="card bg-pink-400">
									<div class="card-body">
										<div class="d-flex">
											<h3 class="font-weight-semibold mb-0"><?=$total_pemohon->a?></h3>
											<span class="badge bg-teal-800 badge-pill align-self-center ml-auto"></span>
					                	</div>
					                	
					                	<div>
											Total Pendaftar
											<div class="font-size-sm opacity-75"></div>
										</div>
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

                            </div>
                            
                            <div class="col-lg-4">

								<!-- Members online -->
								<div class="card bg-blue-400">
									<div class="card-body">
										<div class="d-flex">
                                            <?php $sisa = $total_kav->a - $total_pemohon->a; ?>
											<h3 class="font-weight-semibold mb-0"><?=$sisa?></h3>
											<span class="badge bg-teal-800 badge-pill align-self-center ml-auto"></span>
					                	</div>
					                	
					                	<div>
											Sisa Kavling
											<div class="font-size-sm opacity-75"></div>
										</div>
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>

							
                            
                            
                            
						</div>
						<!-- /quick stats boxes -->

    </div>
</div>
<!-- /dashboard content -->

</div>
<!-- /content area -->


<?php require(__DIR__ . '/template/footer.php') ?>
            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>