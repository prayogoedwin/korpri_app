	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center pt-0">

				<!-- Registration form -->
				
                <?php 
                $attributes = array('class' => 'flex-fill');
                echo form_open_multipart('welcome/daftar', $attributes); 
                ?>
					<div class="row">
						<div class="col-lg-6 offset-lg-3">
							<div class="card mb-0">
								<div class="card-body">
									<div class="text-center mb-3">
                                    <img src="<?=base_url()?>assets/jatengs.png" width="100" >
										<!-- <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i> -->
										<!-- <h5 class="mb-0">Data dibawah adalah data </h5>
										<span class="d-block text-muted">All fields are required</span> -->
                                    </div>
                                    
									<!-- <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" value="<?=$nama?>">
										<div class="form-control-feedback">
											<i class="icon-user-check text-muted"></i>
										</div>
                                    </div>
                                     -->
                                    <div class="row">
										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                               
                                            <input  required readonly type="text" class="form-control" placeholder="Nama Lengkap" name="nama" value="<?=$nama?>">
										<div class="form-control-feedback">
											<i class="icon-user-check text-muted"></i>
										</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                            <input readonly required type="text" class="form-control" placeholder="NIP" name="nip" value="<?=$nip?>">
                                            <input readonly type="hidden" class="form-control" placeholder="NIP" name="tgl_sk_cpns" value="<?=$tgl_sk_cpns?>">
                                            <input readonly type="hidden" class="form-control" placeholder="NIP" name="tgl_sk_pns" value="<?=$tgl_sk_pns?>">
												<div class="form-control-feedback">
													<i class="icon-user-check text-muted"></i>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                               
                                            <input readonly type="text" class="form-control" placeholder="NIK" name="nik" value="<?=$nik?>">
												<div class="form-control-feedback">
													<i class="icon-user-check text-muted"></i>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                            <input readonly type="text" class="form-control" placeholder="Golongan" name="gol" value="<?=$gol?>">
												<div class="form-control-feedback">
													<i class="icon-user-check text-muted"></i>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                            <input readonly type="text" class="form-control" placeholder="Tempat Lahir" name="tm_lhr" value="<?=$tm_lhr?>">
												<div class="form-control-feedback">
													<i class="icon-user-check text-muted"></i>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                            <input readonly type="text" class="form-control" placeholder="Tanggal Lahir" name="tg_lhr" value="<?=$tg_lhr?>">
												<div class="form-control-feedback">
													<i class="icon-user-check text-muted"></i>
												</div>
											</div>
										</div>
										
                                    </div>

                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                    <textarea readonly class="form-control" name="alamat" rows="5"><?=$alamat?></textarea>
                                    <div class="form-control-feedback">
											<i class="icon-user-check text-muted"></i>
										</div>
                                    </div>


                                    <div class="row">
										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="email" class="form-control" required placeholder="Email Lahir" name="email">
												<div class="form-control-feedback">
													<i class="icon-envelope text-muted"></i>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="number" class="form-control" required placeholder="No Telp/HP/Whatsapp" name="no_hp" >
												<div class="form-control-feedback">
													<i class="icon-phone2 text-muted"></i>
												</div>
											</div>
										</div>
										
									</div>

									
									

                                     <div class="row">
										<div class="col-md-6">
                                        <div class="form-group form-group-feedback form-group-feedback-right">
                                            <input type="text" class="form-control" required placeholder="Nama Suami / Istri" name="pasangan" >
												<div class="form-control-feedback">
													<i class="icon-man-woman text-muted"></i>
												</div>
											</div>
                                        
										</div>

										<div class="col-md-6">
                                        <div class="form-group">
										
                                        <select data-placeholder="Pilih Kavling Anda" name="pilihan" class="form-control form-control-select2" data-fouc>
                                        <option value="">Pilih Kavling Anda</option> 
                                        <?php foreach($list_kavling as $kav): 
                                         
                                         $cek = cek_ketersediaan($kav->id);

                                         
                                         if($cek > 0){
                                             $disable = 'disabled';
                                             $aa = 'Sudah Dipesan';
                                         }else{
                                            $disable = '';
                                            $aa = '';
                                         }
                                            
                                        ?>    
                                        <option <?=$disable?> value="<?=$kav->id?>"> <?=$kav->kode_kavling?> (Harga = Rp.<?=number_format($kav->harga_jual)?>) <?=$aa?></option> 
                                        <?php endforeach; ?> 
                                        </select>
                                    </div>
										</div>
										
									</div> 
									
									<div class="form-group form-group-feedback form-group-feedback-right">
									<label> Dokumen Pendukung Persyaratan Pengajuan (PDF)</label>
									<input type="file" class="form-control h-auto" required placeholder="File Upload" name="filez" accept="application/pdf" >
                                   
                                    </div>

									<div class="form-group">
										<!-- <div class="form-check">
											<label class="form-check-label">
												<input type="checkbox" class="form-input-styled" checked data-fouc>
												Send me <a href="#">test account settings</a>
											</label>
										</div> -->

										<div class="form-check">
											<label class="form-check-label">
												<input required type="checkbox" class="form-input-styled" data-fouc>
												Sistem ini membutuhkan persetujuan untuk melihat informasi gaji Anda pada Aplikasi SINAGA.<br/>Centang jika Anda menyetujuinya.
											</label>
										</div>

										<!-- <div class="form-check">
											<label class="form-check-label">
												<input type="checkbox" class="form-input-styled" data-fouc>
												Accept <a href="#">terms of service</a>
											</label>
										</div> -->
                                    </div> 
                                    

                                    <!-- button -->

                                    <div class="row">
										<div class="col-md-8">
											
										</div>

										<div class="col-md-4" style="text-align:right">
                                        <button type="submit"  class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b> Daftar Sekarang</button>
										</div>
										
                                    </div>
                                     

									
								</div>
							</div>
						</div>
					</div>
                    <?php form_close() ?>
				<!-- /registration form -->

			</div>
			<!-- /content area -->
    
    </div>
    </div>
    



<?php require(__DIR__ . '/template/footer.php') ?>
<script src="<?=base_url()?>assets/limitless_dark/full/assets/global_assets/js/demo_pages/form_layouts.js"></script>
            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>