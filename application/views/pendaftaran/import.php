<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> IMPORT DATA KETERSEDIAN</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<div class="d-flex justify-content-center">
							 <a href="<?=base_url('assets/format/format_ketersediaan_1.xlsx')?>" class="btn btn-link btn-float text-default"><i class="icon-download"></i><span>Format Excel</span></a>
							<!-- <a href="<?=base_url('ketersediaan/import')?>" class="btn btn-link btn-float text-default"><i class="icon-file-excel"></i> <span>Import Excel</span></a>
							<a href="<?=base_url()?>" class="btn btn-link btn-float text-default"><i class="icon-file-plus"></i> <span>Tambah Satuan</span></a> -->
							
						</div> 
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Import Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>


						<!-- <form action="#"> -->
						<?php 
						
						echo form_open_multipart('kelolaketersediaan/import_act') ?>
							<fieldset class="mb-3">

							<input type="hidden"  class="form-control">

						
						

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tahun Data</label>
		                        	<div class="col-lg-10">
										<select name="tahun" required class="form-control">	
											<?php
											$thn_skr = date('Y');
											for ($x = $thn_skr; $x >= 2010; $x--) {
											?>
												<option value="<?php echo $x ?>"><?php echo $x ?></option>
											<?php
											}
											?>
										</select>

		                            </div>
                                </div>
                                
                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Bulan</label>
		                        	<div class="col-lg-10">
										<select name="bulan" required class="form-control">	
                                        <?php
                                        
                                        $bulan = NULL;
                                        $sel1=NULL;
                                        $bln=array(1=>'Januari','Februari','Maret','April','Mei','Juni','July','Agustus','September','Oktober','November','Desember');
                                        for($bulan=1; $bulan<=12; $bulan++){
                                            if($bulans == $bulan){
                                                $sel1 = 'selected';
                                            }else{
                                                $sel1 = '';
                                            }
                                        if($bulan<=9) { echo "<option $sel1 value='$bulan'>$bln[$bulan]</option>"; 
                                            
                                        }else { echo "<option $sel1 value='$bulan'>$bln[$bulan]</option>"; }
                                        }
                                        ?>
										</select>

		                            </div>
                                </div>
                                
                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Komoditi</label>
		                        	<div class="col-lg-10">
										<select name="komoditas" required class="form-control">	
											<?php
                                            foreach($komoditas as $kmdt):
                                                
                                                echo "<option value='$kmdt->id'>$kmdt->nama</option>"; 

                                            endforeach;
											?>
										</select>

		                            </div>
                                </div>

								

								

								<!-- <div class="form-group row">
									<label class="col-form-label col-lg-2">Tanggal Proposal</label>
									<div class="col-lg-10">
										<input type="date" required name="tanggal" class="form-control">
									</div>
								</div> -->

								

								<div class="form-group row">
									<label class="col-form-label col-lg-2">File Excel</label>
									<div class="col-lg-10">
										<input type="file" required name="file" class="form-control h-auto">
									</div>
								</div>

								
							</fieldset>

						


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

            <?php 
            $this->load->view('template/footer');
          
			?>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>