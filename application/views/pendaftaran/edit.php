<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> VERIFIKASI</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<div class="d-flex justify-content-center">
							<!-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>-->
							<!-- <a href="<?=base_url('ketersediaan/import')?>" class="btn btn-link btn-float text-default"><i class="icon-file-excel"></i> <span>Import Excel</span></a>
							<a href="<?=base_url()?>" class="btn btn-link btn-float text-default"><i class="icon-file-plus"></i> <span>Tambah Satuan</span></a> -->
							
						</div> 
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Verifikasi Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
						<p class="mb-4"></p>

						<!-- <form action="#"> -->
						<?php 
						
						echo form_open('kelolapendaftaran/act_verifikasi') ?>
							<fieldset class="mb-3">

							<input type="hidden"  class="form-control">
						

								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Nama PNS</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="nama" class="form-control" readonly value="<?=$pns->nama?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">NIP</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="nama" class="form-control" readonly value="<?=$pns->nip?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tempat Lahir</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="nama" class="form-control" readonly value="<?=$pns->tm_lhr?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tanggal Lahir</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="nama" class="form-control" readonly value="<?=$pns->tg_lhr?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Alamat</label>
		                        	<div class="col-lg-10">
										<textarea class="form-control" rows="5" readonly><?=$pns->alamat?></textarea>
                                    <!-- <input type="text" required name="nama" class="form-control" readonly value="<?=$pns->alamat?>"> -->
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Instansi</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="instansi" class="form-control" readonly value="<?=$pns->instansi?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Unit Kerja</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="unit" class="form-control" readonly value="<?=$pns->unit?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Tanggal SK PNS</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="tgl_sk_pns" class="form-control" readonly value="<?=$pns->tgl_sk_pns?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Pangakat Golongan</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="gol" class="form-control" readonly value="<?=$pns->gol?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">NIK</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="nik" class="form-control" readonly value="<?=$pns->nik?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Email</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="email" class="form-control" readonly value="<?=$pns->email?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Telp/HP/WA</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="telp" class="form-control" readonly value="<?=$pns->telp?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Nama Suami/Istri</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="pasangan" class="form-control" readonly value="<?=$pns->pasangan?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Gaji per <?=$pns->bulan?>-<?=$pns->tahun?> Rp.</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="gaji" class="form-control" readonly value="<?=number_format($pns->gaji)?>">
		                            </div>
                                </div>

                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Kavling Pilihan</label>
		                        	<div class="col-lg-10">
										
                                    <input type="text" required name="kavling" class="form-control" readonly value="<?=$pns->pilihanmnya?>">
		                            </div>
                                </div>

                                
                                
                                
                                
                               
                                <input type="hidden" required name="id" class="form-control" readonly value="<?=$pns->id?>">
								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Status</label>
		                        	<div class="col-lg-10">
										<select name="status" required class="form-control">	
                                            <?php 
                                            
                                            $xxx = $this->db->query("SELECT * FROM `status`")->result(); 

                                            foreach($xxx as $xx){
                                            if($pns->status == $xx->id ){
                                                $act = 'selected';
                                            }else{
                                                $act = '';
                                            }
                                            
                                            ?>
                                            
                                            <option value="<?=$xx->id?>" <?=$act?> ><?=$xx->nama?></option>

                                            <?php } ?>
                                           
										</select>

		                            </div>
                                </div>

								

								

								<!-- <div class="form-group row">
									<label class="col-form-label col-lg-2">Tanggal Proposal</label>
									<div class="col-lg-10">
										<input type="date" required name="tanggal" class="form-control">
									</div>
								</div> -->

								

								<!-- <div class="form-group row">
									<label class="col-form-label col-lg-2">File Excel</label>
									<div class="col-lg-10">
										<input type="file" required name="excel" class="form-control h-auto">
									</div>
								</div> -->

								
							</fieldset>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

            <?php 
            $this->load->view('template/footer');
          
			?>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>