<!-- Main content -->
<div class="content-wrapper">




<!-- Content area -->
<div class="content pt-0">

				
<!-- Dashboard content -->
<div class="row">
    <div class="col-xl-12">
        <!-- Quick stats boxes -->
        <div class="row">
            <div class="col-lg-12">
                <br/>

<?php if($this->session->userdata('akses') != 3){ ?>
            <!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Edit <?=nama_tipe_image($alldata->tipe)->name?></h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
						<p class="mb-4"></p>

						<!-- <form action="#"> -->
						<?php 
						echo form_open('kelolaprofil/edit_struktur') ?>
							<fieldset class="mb-3">



								<div class="form-group row">
									<label class="col-form-label col-lg-2">File (PNG/JPG)</label>
									<div class="col-lg-10">
                                    <input type="file" class="form-control h-auto">
									</div>
								</div>

								
								
								

								
							</fieldset>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
                <!-- /form inputs -->
            <?php } ?>
            
     

              <img class="img-responsive" style="width:970px" src="<?=base_url($alldata->nama_file)?>">
             
            

            </div>
        </div>
        <!-- /quick stats boxes -->

    </div>
</div>
<!-- /dashboard content -->

</div>
<!-- /content area -->


<?php 
$this->load->view('template/footer');
?>

            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>