<?php
    /* Mengambil query report*/
    foreach($komoditas_kkn as $kmdt){
		    $komoditi[]   = $kmdt->komoditi;
        $sedia[]      = (float) $kmdt->ketersediaan;
        $butuh[]      = (float) $kmdt->butuhan;
        $neraca[]     = (float) ($kmdt->ketersediaan - $kmdt->butuhan);
    }
	/* end mengambil query*/
?>

<style>
	.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#container {
    height: 300px;
}
#container2 {
    height: 300px;
}

#container3 {
    height: 300px;
}
#container4 {
    height: 300px;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
</style>

<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">

				<!-- Main charts -->
				<div class="row">
					<div class="col-xl-12">

						<!-- Traffic sources -->
						<div class="card" >
							<div class="card-header header-elements-inline">
								<h6 class="card-title">Neraca Ketersediaan Komoditas Pangan Bulan September 2020</h6>
								<div class="header-elements">
									<div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
										<!-- <label class="form-check-label">
											Live update:
											<input type="checkbox" class="form-input-switchery" checked data-fouc>
										</label> -->
									</div>
								</div>
							</div>

							

						
							<div id="container"></div>
							

						</div>
						<!-- /traffic sources -->

					</div>

					

					</div> 
				</div>
				<!-- /main charts -->

				


				

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


            <?php 
            $this->load->view('template/footer');
           
			?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<script>
Highcharts.chart('container', {
	colors: ['#E8C731', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
		'#55BF3B', '#DF5353', '#7798BF', '#aaeeee'
	],
    chart: {
		backgroundColor: '#353F53',
		type: 'column',
    },
    exporting: { enabled: false },
    title: {
		style: {
				color: '#FFF',
				fontWeight: 'bold',
				fontSize: '12px',
				fontFamily: 'Trebuchet MS, Verdana, sans-serif'

			},
        text: ''
    },
    subtitle: {
		style: {
				color: '#FFF',
				fontWeight: 'normal',
				fontSize: '12px',
				fontFamily: 'Trebuchet MS, Verdana, sans-serif'

			},
        text: ''
    },
    xAxis: {
		labels: {
            style: {
                color: '#FFF'
            }
        },
        categories: <?=json_encode($komoditi);?>,
        crosshair: true
    },
    yAxis: {
		labels: {
            style: {
                color: '#FFF'
            }
        },
        min: 0,
        title: {
			  style: {
				color: '#FFF',
				// fontWeight: 'bold',
				fontSize: '12px',
				fontFamily: 'Trebuchet MS, Verdana, sans-serif'

			},
            text: 'Jumlah (Ton)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} Ton</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
        }
    },
    series: [
            { name: 'Ketersediaam', data: <?=json_encode($sedia)?> },
            { name: 'Kebutuhan', data: <?=json_encode($butuh)?> },
            { name: 'Neraca', data: <?=json_encode($neraca)?> }
        ],
});
</script>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>