<!-- Main content -->
<div class="content-wrapper">


<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> DATA KEBUTUHAN</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<div class="d-flex justify-content-center">
							<!-- <a href="<?=base_url('kelolaketersediaan/dashboard')?>" class="btn btn-link btn-float text-default"><i class="icon-stats-bars2"></i><span>Grafik</span></a>
							<a href="<?=base_url('kelolaketersediaan/kebutuhan_tambah')?>" class="btn btn-link btn-float text-default"><i class="icon-file-plus"></i> <span>Tambah Satuan</span></a>
							<a href="<?=base_url('kelolaketersediaan/kebutuhan_import')?>" class="btn btn-link btn-float text-default"><i class="icon-file-excel"></i> <span>Import Excel 1</span></a> -->
							<a href="<?=base_url('kelolaketersediaan/kebutuhan_import_all')?>" class="btn btn-link btn-float text-default"><i class="icon-file-excel"></i> <span>Import Excel</span></a>
							
							
						</div> 
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				
				<!-- Basic initialization -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Tabel Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<div class="card-body">
                    <?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>
					</div>

					<table id="$datatable" class="table datatable-button-html5-basic">
						<thead style="text-align:left">
							<tr style="text-align:left">
								<th>No</th>
								
                                <th>Kabupaten/Kota</th>
                                <th>Komoditas</th>
                                <th>Jumlah Ketersediaan</th>
                              
                                <th>Tahun</th>
                                <!-- <th>Aksi</th> -->
                                
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				<!-- /basic initialization -->

			</div>

			</div>
			<!-- /content area -->


			<?php 
            $this->load->view('template/footer');
            $this->load->view('template/datatable');
			?>

			<script>
				// Basic Datatable examples
var _componentDatatableButtonsHtml5 = function() {
    if (!$().DataTable) {
        console.warn('Warning - datatables.min.js is not loaded.');
        return;
    }

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
        }
    });

  

    // Basic initialization
    var tabel = $('.datatable-button-html5-basic').DataTable({
        buttons: {            
            dom: {
                button: {
                    className: 'btn btn-light'
                }
            },
            buttons: [
                'excelHtml5'
            ]
        },
		"pageLength": 35,
        'scrollX'   : true,
        'data'      : <?=json_encode($datatable);?>,
        'columns'   : [
                        { data: 'no' },
                        { data: 'kota' },
                        { data: 'komoditi' },
                        { data: 'butuh' },
                      
                        { data: 'tahun' },
                        // { data: 'action' }
                      ]
        });
        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

		};
		</script>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>