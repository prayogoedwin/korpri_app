<!-- Main content -->
<div class="content-wrapper">

<!-- Page header -->
<div class="page-header border-bottom-0">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> IMPORT DATA KEBUTUHAN V2</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none mb-3 mb-md-0">
						<div class="d-flex justify-content-center">
							 <a href="<?=base_url('assets/format/format_kebutuhan_2.xlsx')?>" class="btn btn-link btn-float text-default"><i class="icon-download"></i><span>Format Excel</span></a>
							 <a href="<?=base_url('assets/format/kode_komoditas.xlsx')?>" class="btn btn-link btn-float text-default"><i class="icon-file-excel"></i> <span>Kode Komoditas</span></a>
							<!-- <a href="<?=base_url()?>" class="btn btn-link btn-float text-default"><i class="icon-file-plus"></i> <span>Tambah Satuan</span></a> -->
							
						</div> 
					</div>
				</div>
			</div>
            <!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Form inputs -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Import Data</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
					<?php
						$message = $this->session->flashdata('message');
						$info = $this->session->flashdata('info');
                        if (isset($message)) { ?>
                           
						 <!-- Solid alert -->
						 <div class="alert bg-<?=$info?> alert-styled-left alert-dismissible">
						 <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
						 <span class="font-weight-semibold"><?=strtoupper($info)?>! &nbsp;</span><?=$message?>
				    </div>
					<!-- /solid alert -->
					
					<?php 	} ?>


						<!-- <form action="#"> -->
						<?php 
						
						echo form_open_multipart('kelolaketersediaan/kebutuhan_import_all_act') ?>
							<fieldset class="mb-3">

							<input type="hidden"  class="form-control">

					
								

								<div class="form-group row">
									<label class="col-form-label col-lg-2">File Excel</label>
									<div class="col-lg-10">
										<input type="file" required name="file" class="form-control h-auto">
									</div>
								</div>

								
							</fieldset>

						


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<!-- /form inputs -->

			</div>
			<!-- /content area -->

            <?php 
            $this->load->view('template/footer');
          
			?>


            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>