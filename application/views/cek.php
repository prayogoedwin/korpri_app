<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Content area -->
    <div class="content d-flex justify-content-center align-items-center pt-0">

        <!-- Login form -->
      
        <?php echo form_open('welcome/pendaftaran')?>
            <div class="card mb-0">
                <div class="card-body">
                    <div class="text-center mb-3">
                        <img src="<?=base_url()?>assets/jatengs.png" width="100" >
                        <!-- <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i> -->
                        <h5 class="mb-0">PENDAFTARAN PERUMAHAN KORPRI</h5>
                        <span class="d-block text-muted">Masukkan NIP Untuk Mendaftar</span>
                        <?php
                        $message = $this->session->flashdata('message');
                        if (isset($message)) {
                            echo "<span style='color:red'>".$message."</span>";
						}
					?>
                    </span>
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left">
                        <input type="text" name="nip" class="form-control" placeholder="NIP">
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                    </div>

                   

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>

                    <!-- <div class="text-center">
                        <a href="login_password_recover.html">Forgot password?</a>
                    </div> -->
                </div>
            </div>
        <?php form_close() ?>
    
        <!-- /login form -->

    </div>
    <!-- /content area -->
    
    </div>
    </div>
    



<?php require(__DIR__ . '/template/footer.php') ?>
            


            </div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>