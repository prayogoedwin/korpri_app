<?php 

function is_login(){
    $ci = get_instance();
    if($ci->session->userdata('id') == null){
        redirect('welcome');
    }else{
        return true;
    }
}

function cek_ketersediaan($id){
    $ci = get_instance();
    return $ci->db->query("select count(id) as jml FROM list_pemohon WHERE kavling_pilihan = '$id'")->row()->jml;
    
}

function cek_one($satu, $url){
    $class1 = '';

        if($satu == $url){
            $class1 = 'active';
            echo $class1;
        }else{
            $class1 = '';
            echo $class1;
        }
    }



function status($kode){
    switch ($kode) {
        case '1':
            return 'Menunggu Proses Verifikasi';
            break;
        case '2':
            return 'Lolos Proses Verifikasi';
            break;
        case '3':
            return 'Tidak Lolos Proses Verifikasi';
            break;
        default:
            return '-';
            break;
    }
}

function post_data($endpoint, $json){

    $url = "http://srv.bkd.jatengprov.go.id/mobile1/api/".$endpoint;
    // $postData = '';
    //create name value pairs seperated by &
    // foreach($params as $k => $v) 
    // { 
    //   $postData .= $k . '='.$v.'&'; 
    // }
    // rtrim($postData, '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);  
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_ENCODING, '');
    $response = curl_exec($ch);
    $err = curl_error($ch);
    
    curl_close($ch);

    if ($err) {
        // echo "cURL Error #:" . $err;
        return $err;
    } else {
        // $hsl = json_decode($response, TRUE);
        return $response;
        // $status = $hsl['status'];
        // if($status){
        //     // return TRUE;
        //     return $response;
        // } else {
        //     // return FALSE;
        //     return $response;
        // }
    }
}

?>