# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: perum_korpri
# Generation Time: 2021-04-25 21:43:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table list_pemohon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `list_pemohon`;

CREATE TABLE `list_pemohon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `tm_lhr` varchar(255) DEFAULT NULL,
  `tg_lhr` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `instansi` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `tgl_sk_cpns` varchar(255) DEFAULT NULL,
  `tgl_sk_pns` varchar(255) DEFAULT NULL,
  `gol` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `pasangan` varchar(255) DEFAULT NULL,
  `gaji` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `kavling_pilihan` int(11) DEFAULT NULL,
  `persyaratan` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `list_pemohon` WRITE;
/*!40000 ALTER TABLE `list_pemohon` DISABLE KEYS */;

INSERT INTO `list_pemohon` (`id`, `nama`, `nip`, `tm_lhr`, `tg_lhr`, `alamat`, `instansi`, `unit`, `tgl_sk_cpns`, `tgl_sk_pns`, `gol`, `nik`, `email`, `telp`, `pasangan`, `gaji`, `bulan`, `tahun`, `kavling_pilihan`, `persyaratan`, `status`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(13,'SANGAJI RIFQIANTO, S.Kom.,M.Kom.','198809022011011004','SEMARANG','02 September 1988','JL. AIRA III AF.IV/7\r\nRt. 006 Rw. 012\r\nDesa (Kelurahan) : Bringin\r\nKecamatan : Ngaliyan\r\nKabupaten/Kota : Semarang','BADAN KEPEGAWAIAN DAERAH','BADAN KEPEGAWAIAN DAERAH','17 March 2011','29 March 2012','Penata Muda Tingkat I','33.7406.020988.0007','intechindo313@gmail.com','14045','qweqwe',3853900,11,2020,10,'/assets/upload/persyaratan/11_2020/8828d2f4bc7d63ebbf3d531cf618e20a.pdf',1,'2020-11-26 19:55:51',NULL,NULL);

/*!40000 ALTER TABLE `list_pemohon` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table list_produk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `list_produk`;

CREATE TABLE `list_produk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `periode_id` int(11) DEFAULT NULL,
  `blok` varchar(11) DEFAULT NULL,
  `kode_kavling` varchar(11) DEFAULT NULL,
  `luas_kavling` float DEFAULT NULL,
  `harga_jual` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `list_produk` WRITE;
/*!40000 ALTER TABLE `list_produk` DISABLE KEYS */;

INSERT INTO `list_produk` (`id`, `periode_id`, `blok`, `kode_kavling`, `luas_kavling`, `harga_jual`)
VALUES
	(3,1,'A','A-1 ',120,397388500),
	(4,1,'A','A-2 ',120,397388500),
	(5,1,'A','A-3 ',160,493558000),
	(6,1,'A','A-4 ',156,483941050),
	(7,1,'A','A-5 ',156,483941050),
	(8,1,'A','A-6 ',156,483941050),
	(9,1,'A','A-7 ',156,483941050),
	(10,1,'A','A-8 ',160,493558000),
	(11,1,'A','A-9 ',120,397388500),
	(12,1,'A','A-10 ',123.5,405803331),
	(13,1,'A','A-11 ',123.5,405803331),
	(14,1,'A','A-12 ',123.5,405803331),
	(15,1,'A','A-13 ',123.5,405803331),
	(16,1,'A','A-14 ',119.6,396426805),
	(17,1,'A','A-15 ',123.5,405803331),
	(18,1,'A','A-16 ',123.5,405803331),
	(19,1,'A','A-17 ',124,407005450),
	(21,1,'B','B-1',73,284389338),
	(22,1,'B','B-2',72,281985100),
	(23,1,'B','B-3',72,281985100),
	(24,1,'B','B-4',78,296410525),
	(25,1,'B','B-5',80,301219000),
	(26,1,'B','B-6',80,301219000),
	(27,1,'B','B-7',80,301219000),
	(28,1,'B','B-8',80,301219000),
	(29,1,'B','B-9',80,301219000),
	(30,1,'B','B-10',80,301219000),
	(31,1,'B','B-11',80,301219000),
	(32,1,'B','B-12',80,301219000),
	(33,1,'B','B-13',80,301219000),
	(34,1,'B','B-14',80,301219000),
	(35,1,'B','B-15',80,301219000),
	(36,1,'B','B-16',80,301219000),
	(37,1,'B','B-17',80,301219000),
	(38,1,'B','B-18',80,301219000),
	(39,1,'B','B-19',80,301219000),
	(40,1,'B','B-20',80,301219000),
	(41,1,'B','B-21',80,301219000),
	(42,1,'B','B-22',80,301219000),
	(43,1,'B','B-23',80,301219000),
	(44,1,'B','B-24',80,301219000),
	(45,1,'B','B-25',80,301219000),
	(46,1,'B','B-26',80,301219000),
	(47,1,'B','B-27',80,301219000),
	(48,1,'B','B-28',80,301219000),
	(49,1,'B','B-29',80,301219000),
	(50,1,'B','B-30',80,301219000),
	(51,1,'B','B-31',80,301219000),
	(52,1,'B','B-32',80,301219000),
	(53,1,'B','B-33',80,301219000),
	(54,1,'B','B-34',80,301219000),
	(55,1,'B','B-35',80,301219000),
	(56,1,'B','B-36',80,301219000),
	(57,1,'B','B-37',80,301219000),
	(58,1,'B','B-38',80,301219000),
	(59,1,'B','B-39',109,370941888),
	(60,1,'B','B-40',99,346899513),
	(61,1,'B','B-41',72,281985100),
	(62,1,'B','B-42',72,281985100),
	(63,1,'B','B-43',72,281985100),
	(64,1,'B','B-44',72,281985100),
	(65,1,'B','B-45',72,281985100),
	(66,1,'B','B-46',72,281985100),
	(67,1,'B','B-47',77,294006288),
	(68,1,'B','B-48',71,279580863),
	(69,1,'B','B-49',76,291602050),
	(70,1,'B','B-50',82,306027475),
	(71,1,'B','B-51',88,320452900),
	(72,1,'B','B-52',93,332474088),
	(73,1,'B','B-53',128,416622400),
	(74,1,'B','B-54',135,433452063),
	(75,1,'B','B-55',174,527217325),
	(76,1,'B','B-56',176,532025800),
	(77,1,'B','B-57',181,544046988),
	(78,1,'B','B-58',139,443069013),
	(79,1,'B','B-59',140,445473250),
	(80,1,'B','B-60',140,445473250),
	(81,1,'B','B-61',136,435856300),
	(82,1,'B','B-62',102,354112225),
	(83,1,'B','B-63',89,322857138),
	(84,1,'B','B-64',97,342091038),
	(85,1,'B','B-65',102,354112225),
	(86,1,'B','B-66',89,322857138),
	(87,1,'B','B-67',86,315644425),
	(88,1,'B','B-68',182,546451225),
	(89,1,'B','B-69',210,613769875),
	(90,1,'B','B-70',124,407005450),
	(91,1,'B','B-71',124,407005450),
	(92,1,'B','B-72',90,325261375),
	(93,1,'B','B-73',84,310835950),
	(94,1,'B','B-74',88,320452900),
	(95,1,'B','B-75',90,325261375),
	(96,1,'B','B-76',84,310835950),
	(97,1,'B','B-77',91,327665613),
	(98,1,'B','B-78',90,325261375),
	(99,1,'B','B-79',72,281985100),
	(100,1,'B','B-80',86,315644425),
	(101,1,'B','B-81',85.5,314442306),
	(102,1,'B','B-82',77,294006288),
	(103,1,'C','C-1 ',128,416622400),
	(104,1,'C','C-2 ',92,330069850),
	(105,1,'C','C-3 ',92,330069850),
	(106,1,'C','C-4 ',72,281985100),
	(107,1,'C','C-5 ',72,281985100),
	(108,1,'C','C-6 ',72,281985100),
	(109,1,'C','C-7 ',72,281985100),
	(110,1,'C','C-8 ',72,281985100),
	(111,1,'C','C-9 ',72,281985100),
	(112,1,'C','C-10 ',72,281985100),
	(113,1,'C','C-11 ',72,281985100),
	(114,1,'C','C-12 ',92,330069850),
	(115,1,'C','C-13 ',92,330069850),
	(116,1,'C','C-14 ',113,380558838),
	(117,1,'C','C-15 ',134,431047825),
	(118,1,'C','C-16 ',73.5,285591456),
	(119,1,'C','C-17 ',73.5,285591456),
	(120,1,'C','C-18 ',73.5,285591456),
	(121,1,'C','C-19 ',73.5,285591456),
	(122,1,'C','C-20 ',73.5,285591456),
	(123,1,'C','C-21 ',73.5,285591456),
	(124,1,'C','C-22 ',73.5,285591456),
	(125,1,'C','C-23 ',85.8,315043366),
	(126,1,'C','C-24 ',73.5,285591456),
	(127,1,'C','C-25 ',73.5,285591456),
	(128,1,'C','C-26 ',73.5,285591456),
	(129,1,'C','C-27 ',87,318048663),
	(130,1,'C','C-28 ',87,318048663),
	(131,1,'C','C-29 ',73.5,285591456),
	(132,1,'C','C-30 ',73.5,285591456),
	(133,1,'C','C-31 ',73.5,285591456),
	(134,1,'C','C-32 ',73.5,285591456),
	(135,1,'C','C-33 ',73.5,285591456),
	(136,1,'C','C-34 ',73.5,285591456),
	(137,1,'C','C-35 ',73.5,285591456),
	(138,1,'C','C-36 ',73.5,285591456),
	(139,1,'C','C-37 ',73.5,285591456),
	(140,1,'C','C-38 ',73.5,285591456),
	(141,1,'C','C-39 ',87,318048663),
	(142,1,'C','C-40 ',87,318048663),
	(143,1,'C','C-41 ',109,370941888),
	(144,1,'C','C-42 ',75,289197813),
	(145,1,'C','C-43 ',75,289197813),
	(146,1,'C','C-44 ',75,289197813),
	(147,1,'C','C-45 ',75,289197813),
	(148,1,'C','C-46 ',75,289197813),
	(149,1,'C','C-47 ',75,289197813),
	(150,1,'C','C-48 ',75,289197813),
	(151,1,'C','C-49 ',75,289197813),
	(152,1,'C','C-50 ',75,289197813),
	(153,1,'C','C-51 ',109,370941888),
	(154,1,'C','C-52 ',127,414218163),
	(155,1,'C','C-53 ',128,416622400),
	(156,1,'C','C-54 ',130,421430875),
	(157,1,'C','C-55 ',83,308431713),
	(158,1,'C','C-56 ',64.5,263953319),
	(159,1,'C','C-57 ',64.5,263953319),
	(160,1,'C','C-58 ',64.5,263953319),
	(161,1,'C','C-59 ',79,298814763),
	(162,1,'C','C-60 ',106,363729175),
	(163,1,'C','C-61 ',64.5,263953319),
	(164,1,'C','C-62 ',64.5,263953319),
	(165,1,'C','C-63 ',64.5,263953319),
	(166,1,'C','C-64 ',79,298814763),
	(167,1,'C','C-65 ',113,380558838),
	(168,1,'C','C-66 ',91,327665613),
	(169,1,'C','C-67 ',82,306027475),
	(170,1,'C','C-68 ',89,322857138),
	(171,1,'C','C-69 ',84,310835950),
	(172,1,'C','C-70 ',72,281985100),
	(173,1,'C','C-71 ',72,281985100),
	(174,1,'C','C-72 ',72,281985100),
	(175,1,'C','C-73 ',80,301219000),
	(176,1,'C','C-74 ',109,370941888),
	(177,1,'C','C-75 ',107,366133413),
	(178,1,'C','C-76 ',107,366133413),
	(179,1,'C','C-77 ',78,296410525),
	(180,1,'C','C-78 ',78,296410525),
	(181,1,'C','C-79 ',78,296410525),
	(182,1,'C','C-80 ',121,399792738),
	(183,1,'C','C-81 ',101,351707988),
	(184,1,'C','C-82 ',123,404601213),
	(185,1,'C','C-83 ',77,294006288),
	(186,1,'C','C-84 ',66,267559675),
	(187,1,'C','C-85 ',66,267559675),
	(188,1,'C','C-86 ',66,267559675),
	(189,1,'C','C-87 ',100,349303750),
	(190,1,'C','C-88 ',100,349303750),
	(191,1,'C','C-89 ',130,421430875),
	(192,1,'C','C-90 ',130,421430875),
	(193,1,'C','C-91 ',78,296410525),
	(194,1,'C','C-92 ',78,296410525),
	(195,1,'C','C-93 ',78,296410525),
	(196,1,'C','C-94 ',78,296410525),
	(197,1,'C','C-95 ',78,296410525),
	(198,1,'C','C-96 ',78,296410525),
	(199,1,'C','C-97 ',78,296410525),
	(200,1,'C','C-98 ',78,296410525),
	(201,1,'C','C-99 ',78,296410525),
	(202,1,'C','C-100 ',78,296410525),
	(203,1,'C','C-101 ',78,296410525),
	(204,1,'C','C-102 ',78,296410525),
	(205,1,'C','C-103 ',78,296410525),
	(206,1,'C','C-104 ',78,296410525),
	(207,1,'C','C-105 ',78,296410525),
	(208,1,'C','C-106 ',78,296410525),
	(209,1,'C','C-107 ',78,296410525),
	(210,1,'C','C-108 ',78,296410525),
	(211,1,'C','C-109 ',78,296410525),
	(212,1,'C','C-110 ',78,296410525),
	(213,1,'C','C-111 ',130,421430875),
	(214,1,'C','C-112 ',130,421430875),
	(215,1,'C','C-113 ',140,445473250),
	(216,1,'C','C-114 ',84,310835950),
	(217,1,'C','C-115 ',84,310835950),
	(218,1,'C','C-116 ',84,310835950),
	(219,1,'C','C-117 ',84,310835950),
	(220,1,'C','C-118 ',84,310835950),
	(221,1,'C','C-119 ',84,310835950),
	(222,1,'C','C-120 ',84,310835950),
	(223,1,'C','C-121 ',140,445473250),
	(224,1,'C','C-122 ',180,541642750),
	(225,1,'C','C-123 ',104,358920700),
	(226,1,'C','C-124 ',101,351707988),
	(227,1,'C','C-125 ',99,346899513),
	(228,1,'C','C-126 ',100,349303750),
	(229,1,'C','C-127 ',78,296410525),
	(230,1,'C','C-128 ',78,296410525),
	(231,1,'C','C-129 ',78,296410525),
	(232,1,'C','C-130 ',78,296410525),
	(233,1,'C','C-131 ',78,296410525),
	(234,1,'C','C-132 ',78,296410525),
	(235,1,'C','C-133 ',78,296410525),
	(236,1,'C','C-134 ',78,296410525),
	(237,1,'C','C-135 ',83,308431713),
	(238,1,'C','C-136 ',94,334878325),
	(239,1,'C','C-137 ',100,349303750),
	(240,1,'C','C-138 ',138,440664775);

/*!40000 ALTER TABLE `list_produk` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table periode_penjualan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `periode_penjualan`;

CREATE TABLE `periode_penjualan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal_buka` date DEFAULT NULL,
  `tanggal_tutup` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `periode_penjualan` WRITE;
/*!40000 ALTER TABLE `periode_penjualan` DISABLE KEYS */;

INSERT INTO `periode_penjualan` (`id`, `tanggal_buka`, `tanggal_tutup`, `status`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'2020-11-27','2020-12-07',1,NULL,NULL,NULL);

/*!40000 ALTER TABLE `periode_penjualan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;

INSERT INTO `status` (`id`, `nama`)
VALUES
	(1,'Menunggu Proses Verifikasi'),
	(2,'Lolos Proses Verifikasi'),
	(3,'Tidak Lolos Proses Verifikasi');

/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `akses` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `password`, `akses`)
VALUES
	(1,'admin_pr','827ccb0eea8a706c4c34a16891f84e7b',1);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
